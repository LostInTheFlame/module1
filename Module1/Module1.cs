﻿using System;
using System.Linq;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            // Point of entry
        }


        public int[] SwapItems(int a, int b)
        {
            int[] arr = new int[2];
            arr[0] = b;
            arr[1] = a;
            return arr;
        }

        public int GetMinimumValue(int[] input)
        {
            return input.Min();
        }
    }
}
